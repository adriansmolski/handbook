---
title: "Customer Use Case Adoption"
description: "Drive cross-functional alignment around use case adoption as the north star KPI and #1 predictor of Churn & Contraction."
status: active
---

## Purpose

This working group is charged with driving the necessary cross-functional alignment, prioritization, and execution required to deliver improved customer adoption of key use cases within the GitLab platform. Following the 2023-05-16 Sales Key Review, there was a high-priority focus on driving customer use case adoption as the north star KPI and #1 predictor of Churn & Contraction. The formation of this working group was a direct outcome to accelerate efforts to win against GitHub and reduce Churn & Contraction.

**Desired Outcomes**

1. Drive **cross-functional alignment** around use case adoption across sales, customer success, product, data, finance, etc.
1. Mature **existing and new use cases and measures**, including thresholds
1. Build use cases as input into **actions** to drive the business


## Attributes

| Property        | Value      |
|-----------------|------------|
| Date Created    | 2023-05-16 |
| Target End Date | 2024-01-31 |
| Slack           | [#wg-customer-use-case-adoption](https://gitlab.slack.com/archives/C0584NEKSRJ) |
| Google Doc      | [Customer Use Case Adoption Scrum - Agenda](https://docs.google.com/document/d/1WtwXCK1r7hoco5O8oW5SIKiIWtXDr_WOLeWcIaDM7Nk/edit?usp=sharing)  |
| Epic            | [Use Case Adoption Measurement & Improvement](https://gitlab.com/groups/gitlab-com/-/epics/2190)
| Board           | *TBD* |
| Overview & Status | See [Exit criteria](#exit-criteria) below |

## Exit criteria

Note that these goals are aspirational so we set a high bar (and potentially achieving something that is good enough vs. setting a low bar and not achieving something that is good enough).

- [ ] Improve CI Adoption ARR from 40% to 50% by or before the end of FY24

## Roles and Responsibilities

| Working Group Role                           | Person                        | Title                                                      |
|----------------------------------------------|-------------------------------|------------------------------------------------------------|
| Executive Sponsor                            | Sid Sijbrandij                | CEO                                                        |
| DRI                                          | Sherrod Patching              | VP of Customer Success Management                          |
| Facilitator                                  | Michael Arntz                 |                                                            |
| Functional Lead: CSM                         | Sherrod Patching              | VP of Customer Success Management                          |
| Functional Lead: CSM                         | Rachel Fuerst                 | Customer Success Manager, Public Sector                    |
| Functional Lead: Scale CSE                   | Taylor Lund                   | Senior Manager, Customer Success Managers                  |
| Functional Lead: PS                          | Mark Foster                   | Senior Professional Services Engagement Manager            |
| Functional Lead: Product                     | Jackie Porter                 | Director, Product                                          |
| Functional Lead: Growth                      | Paige Cordero                 | Senior Product Manager, Growth                             |
| Functional Lead: Customer Success Operations | Nishant Khanna                | Senior Customer Success Operations Analyst                 |
| Functional Lead: Digital Customer Success    | Michelle Harris               | Senior Program Manager, Customer Programs                  |
| Functional Lead: Data and Insights           | Amie Bright                   | VP of Data and Insights                                    |
| Functional Lead: Enterprise Sales            | *TBD*                         | *TBD*                                                      |
| Functional Lead: Commercial Sales            | Julien Le Postec              | Area Sales Manager Mid Market South EMEA - Commercial Sales|
| Functional Lead: Developer Relations         | Michael Friedrich             | Senior Developer Evangelist                                |
