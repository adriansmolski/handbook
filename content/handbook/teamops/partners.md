---
title: "TeamOps Partners"
description: How to become a TeamOps Representative
canonical_path: "/handbook/teamops/partners/"
images:
    - /images/opengraph/all-remote.jpg
---

A TeamOps Partnership is a cooperation between TeamOps and future-of-work advocates, vendors, tools, or consultants to support employers in the process of understanding, measuring, and adopting virtual-first ways of working.

In this partnership, TeamOps provides the education and assessment resources required for change management, then converts learners into sales leads for the partners.

## Partnership Purpose

Building on the TeamOps mission to define universal new world of work operational standards, this corresponding partnership program has been created with the intent to simplify and streamline digital-first change management processes.

Over time, in addition to supporting the business goals of both GitLab and its partners, the collaboration of these partners will also contribute to the following benefits for the future of work industry:
- **Centralize Knowledge and Resources** – Universal operational standards will reflect and unify the goals of the diverse branches of the future of work industry (remote work, artificial intelligence, DEI (diversity, equity, and inclusion), freelancing, job sharing, four-day work weeks, etc.), and the categories of thought leadership within each (consultants, evangelists, tools, researchers, etc.)
- **Establish Informational Consistency** – The missions and success of the various branches of the future of work are often misrepresented in media coverage, marketing campaigns, and socially-circulated myths. The authoritative messaging of TeamOps will serve as a source of truth as a credible counterargument.
- **Lead Industry Growth and Development** – Building on the iconic reputation and thought leadership of GitLab, TeamOps will continue to pioneer emerging topics, and provide opportunities for continuing education and career development for thought leaders.
- **Endorse Credibility** -  GitLab has solidified itself as the go-to authority with its proven operating model. Strengthening the industry's understanding of modern work efficiency. Therefore, by becoming a TeamOps partner, industry professionals, vendors, and consultants will be recognized and endorsed by TeamOps, establishing their credibility as trusted advisors.

## Partnership Process

When TeamOps learners are completing their TeamOps Practitioner certifications, they often are looking for more change management support than GitLab is equipped to provide. To increase the actionability of TeamOps tenets, they are guided to partnership resources that provide the necessary support to qualify for certification. That process is as follows:
- **Assessment** - Collection of the TeamOps Assessment response via form
- **Results** - Delivery of the TeamOps Assessment results vai a custom slide deck and video call
- **Match** - If interest in services is confirmed, matching criteria is collected via form and compared to partner requirements
- **Referral** - Top 3 partner matches are delivered to the customer, then followed with warm intro emails

## Partner Profile

A TeamOps partner is any professional (individuals or businesses) that applies the TeamOps principles and tenets as the ideal for universal virtual-first operational standards (including handbook-first, asynchronous-first, autonomy-first, and results-first ways of working), including:
- *Consultants* – Change management advisors that guide businesses through organizational adoption of TeamOps standards
- *Tools* - Products and services that facilitate the implementation of TeamOps standards

Our evaluation and approval process for TeamOps Partners is based on the following criteria:
- *Proven Track Record:* We look for partners with a history of successfully assisting companies to align with universal virtual-first operational standards, including handbook-first, asynchronous-first, autonomy-first, and results-first ways of working.
- *Published Case Studies:* We will first consider applicants who have published case studies that demonstrate expertise and successful interventions in their field of work.
- *Geographic Location & Language:* We aim to cater to a diverse range of markets. Therefore, the geographic location and service language will be evaluated to determine if they meet the needs of our market.


## Partnership Value

The TeamOps Assessment defines and measures universal standards for successful virtual-first ways of working.

Consultants can rely on the TeamOps Assessment results to inform their service recommendations and SOW design, including:
- Low-scoring categories inform what type of consultancy is needed
- High scoring categories can be skipped or deprioritized
- Roadmap results will reveal critical change management steps that may have been inadvertently missed and/or what should be prioritized for completion next
- Customer profile data (company size, budget, location, languages, etc) will filter consultant recommendations, so every lead is qualified as a match for their target market

Consultants, tools, and advocates for the future of work need industry standards to diagnose adoption gaps and communicate the goals of virtual-first operations and ways of working (handbook-first, async-first, autonomy-first, and results-first).
= The partners’  clients (flexibility adopters) are struggling with hybrid and distributed working, and not understanding why. Change agents within those organizations aren’t aligned on what needs to be done and in what order.
- The credibility of industry partners’ arguments are unfounded. Arguments and cases for change ultimately are based on subjective opinions, private research, and based on personal experience.
- TeamOps has the metrics to diagnose what needs to be done to achieve success and sustainability, and can equip consultants with a prioritized to-do list for change management.

| What's in it for the Partner? | What's in it for GitLab? |
| ----- | ----- |
| Lead generation and qualification | Minimal marketing needs |
| Access to universal standards and guidelines for successful, sustainable virtual-first ways of working to measure client success and clarify service goals | Minimal overhead costs (removing needs for internal consultants and facilitators) |
| Standardized assessment system and automated tool for status measurement, benchmarking, and service recommendation | More scalable growth plan than consulting |

## Partner Responsibilities

| The partner agrees to… | TeamOps/GitLab agrees to… |
| ----- | ----- |
| Provide TeamOps with matching criteria preferences for lead sourcing and keep up to date | Conduct TeamOps Assessments and provide result summaries to recommended (matched) partners for sales lead qualification |
| Provide TeamOps a way to track the lead within the company sales funnel | Enforce an unbiased, predetermined matching process for sales leads based on the alignment between responses to the TeamOps Partner Matching Criteria forms |
| Confirm that any/all leads generated through TeamOps-sourced speaking engagements are funneled through the TeamOps sales workstream | Recommend partners for sales leads based on responses to the TeamOps Partner Matching Criteria forms |
| Pay TeamOps a 10% referral fee for any sales closed | Manage the invoicing and procurement process for referral fees |
| Review all critical Partner communications and updates marked “important” in the subject line | Invite a partner to local speaking and facilitation opportunities based on unbiased, predetermined matching formulas |
| Complete the TeamOps Partner certification (when launched) to maintain Partnership credentials | Manage and maintain equitable partner community communication |
